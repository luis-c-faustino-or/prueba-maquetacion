<!DOCTYPE html>
<html lang="es">

<head>
<title>Prueba Maquetación</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta charset="utf-8">
<meta name="author" content="Luís C Faustino">
<meta name="robots" content="index">
<meta name="description" content="TIDAL Premium llega a España con Vodafone.El futuro de la música ya está aquí, ahora TIDAL de regalo con tu tarifa Vodafone.">

<link rel="stylesheet" href="assets/css/main.css" type="text/css">
<link rel="stylesheet" href="assets/css/media-queries.css" type="text/css">
</head>

<body>

<section class="cover">
  <img class="bg" src="assets/images/bgCover.jpg" alt="">
  <div class="content">
    <img width="152" height="74" class="tidalLogo" src="assets/images/tidalLogo.png" alt="logo tidal">
    <h1>TIDAL Premium llega a España con Vodafone</h1>
    <h2>El futuro de la música ya está aquí, ahora TIDAL de regalo con tu tarifa Vodafone</h2>
    <h3>3 meses gratis con tu tarifa</h3>
    <div class="btn">
      <a href="#">Activar</a>
    </div>
  </div>
</section>

<div class="barList">
  <div class="container">
    <ul>
      <li>Inicio <span class="chevron"></span></li>
      <li>Tidal</li>
    </ul>
  </div>
</div>

<div class="carousel">
  <div class="container">
    <h2>Descubre por qué TIDAL es mucho más que una plataforma de música</h2>
  </div>
  <div class="container">
    <div class="content">
      <div class="inner">
        <div class="module">
            <img src="assets/images/imgModule1.jpg" alt="">
            <h3>Estrenos en Exclusiva</h3>
            <p>TIDAL ha estrenado en exclusiva los mejores álbumes de los dos últimos años y siempre con la mejor calidad de sonido. </p>
        </div>
        <div class="module">
            <img src="assets/images/imgModule2.jpg" alt="">
            <h3>El mejor contenido audiovisual</h3>
            <p>Más de 200 mil videos musicales en alta definición, streaming de conciertos  en directo y series originales.</p>
        </div>
        <div class="module">
            <img src="assets/images/imgModule3.jpg" alt="">
            <h3>Mucho más que música</h3>
            <p>Recomendaciones de conciertos según tus gustos y localización geográfica. Gana entradas para tus conciertos preferidos.</p>
        </div>
      </div>
    </div>
    <div class="bullets">
      <ul>
        <li class="bullets-item bullets-item-1 item-selected"></li>
        <li class="bullets-item bullets-item-2"></li>
        <li class="bullets-item bullets-item-3"></li>
      </ul>
    </div>
  </div>

</div>


<div class="extras">
  <div class="container">
    <h2>Además podrás disfrutar de:</h2>
    <div class="content">
      <ul class="listLeft">
        <li>El más <b>extenso catálogo</b> de música en <b>alta calidad</b>.</li>
        <li>Reproducción de música <b>sin conexión</b>.</li>
        <li><b>Multidispositivo</b>: hasta 3 dispositivos en modo sin conexión.</li>
      </ul>
      <ul class="listRight">
        <li>Podrás compartir música a través de <b>redes sociales</b>.</li>
        <li>Función “<b>Crossfade</b>”: evita el corte entre pistas al reproducir varias canciones.</li>
        <li>Opción “<b>Workout</b>”: propone canciones según los gustos del usuario y el ritmo de carrera.</li>
      </ul>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>
