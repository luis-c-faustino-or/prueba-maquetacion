var bulletPoints = 1;
$('.carousel .content').on( "swiperight", swipeHandler);
$('.carousel .content').on( "swipeleft", swipeHandler);

function swipeHandler(event){
  if (event.type === 'swipeleft' && bulletPoints < 3) {
    bulletPoints++;
  } else if (event.type == 'swiperight' && bulletPoints > 1) {
    bulletPoints--;
  }

  var screenSize = screen.width;
  if (screenSize > 450) {
    switch (bulletPoints) {
      case 1:
        $('.carousel .content .inner').css('left', '0%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-1').addClass('item-selected');
        break;
      case 2:
        $('.carousel .content .inner').css('left', '-21%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-2').addClass('item-selected');
        break;
      case 3:
        $('.carousel .content .inner').css('left', '-42%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-3').addClass('item-selected');
      break;
    }
  } else if (screenSize <= 450) {
    switch (bulletPoints) {
      case 1:
        $('.carousel .content .inner').css('left', '0%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-1').addClass('item-selected');
        break;
      case 2:
        $('.carousel .content .inner').css('left', '-103%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-2').addClass('item-selected');
        break;
      case 3:
        $('.carousel .content .inner').css('left', '-206%');
        $('.carousel .bullets ul .bullets-item').removeClass('item-selected');
        $('.carousel .bullets ul .bullets-item-3').addClass('item-selected');
      break;
    }
  }


}
